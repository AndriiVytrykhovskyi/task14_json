package com.vytrykhovskyi.comparator;

import com.vytrykhovskyi.model.Gems;
import java.util.Comparator;

public class GemsComparator implements Comparator<Gems> {
    @Override
    public int compare(Gems o1, Gems o2) {
        return Integer.compare(o1.getValue(), o2.getValue());
    }
}
