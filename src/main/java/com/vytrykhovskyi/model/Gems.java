package com.vytrykhovskyi.model;

public class Gems {
    private int gemsNumber;
    private String name;
    private String origin;
    private String preciousness;
    private Parameters parameters;
    private int value;

    public Gems(int gemsNumber, String name, String origin, String preciousness, Parameters parameters, int value) {
        this.gemsNumber = gemsNumber;
        this.name = name;
        this.origin = origin;
        this.preciousness = preciousness;
        this.parameters = parameters;
        this.value = value;
    }

    public Gems() {

    }

    public int getGemsNumber() {
        return gemsNumber;
    }

    public void setGemsNumber(int gemsNumber) {
        this.gemsNumber = gemsNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getPreciousness() {
        return preciousness;
    }

    public void setPreciousness(String preciousness) {
        this.preciousness = preciousness;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Gems{" +
                "gemsNumber=" + gemsNumber +
                ", name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", preciousness='" + preciousness + '\'' +
                ", parameters=" + parameters +
                ", value=" + value +
                '}';
    }
}
