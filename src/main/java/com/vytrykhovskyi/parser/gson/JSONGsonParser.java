package com.vytrykhovskyi.parser.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vytrykhovskyi.model.Gems;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

public class JSONGsonParser {
  private Gson gson;

  public JSONGsonParser() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gson = gsonBuilder.create();
  }

  public List<Gems> getGemsList(File json) {
    Gems [] devices = new Gems[0];
    try{
      devices = gson.fromJson(new FileReader(json), Gems[].class);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return Arrays.asList(devices);
  }
}
