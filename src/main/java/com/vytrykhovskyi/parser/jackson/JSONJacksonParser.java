package com.vytrykhovskyi.parser.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vytrykhovskyi.model.Gems;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class JSONJacksonParser {
    private ObjectMapper objectMapper;

    public JSONJacksonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Gems> getGemsList(File jsonFile) {
        Gems[] devices = new Gems[0];
        try {
            devices = objectMapper.readValue(jsonFile, Gems[].class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Arrays.asList(devices);
    }
}
