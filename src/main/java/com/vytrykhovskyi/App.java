package com.vytrykhovskyi;


import com.vytrykhovskyi.comparator.GemsComparator;
import com.vytrykhovskyi.model.Gems;
import com.vytrykhovskyi.parser.gson.JSONGsonParser;
import com.vytrykhovskyi.parser.jackson.JSONJacksonParser;

import com.vytrykhovskyi.validator.JSONValidator;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        File json = new File("src/main/resources/json/GemsJSON.json");
        File schema = new File("src/main/resources/json/GemsJSONSchema.json");

        JSONValidator.validateSchema(schema, json);

        JSONJacksonParser parser = new JSONJacksonParser();

        JSONGsonParser gsonParser = new JSONGsonParser();

        printList(parser.getGemsList(json), "Jackson");

        printList(gsonParser.getGemsList(json), "Gson");
    }

    private static void printList(List<Gems> gemses, String s) {
        Collections.sort(gemses, new GemsComparator());
        System.out.println("\nJSON parser using " + s);
        for (Gems g : gemses) {
            System.out.println(g);
        }
    }
}
